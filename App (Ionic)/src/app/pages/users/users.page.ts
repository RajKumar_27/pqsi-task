import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {


  userForm: FormGroup;
  result: any;
  constructor(private _formBuilder: FormBuilder, public _authService: AuthService,
    private _modalCtrl: ModalController) { }

  ngOnInit() {
    this.userForm = this._formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      avatar: ['https://reqres.in/img/faces/12-image.jpg']
    });
  }

  get userFormValues() {
    return this.userForm.controls;
  }

  create() {
    if (this.userForm.invalid) {
      return;
    }
    this.createUser();
  }

  createUser() {
    const formData = new FormData();
    formData.append('first_name', this.userFormValues.first_name.value);
    formData.append('last_name', this.userFormValues.last_name.value);
    formData.append('email', this.userFormValues.email.value);
    formData.append('avatar', this.userFormValues.avatar.value);
    this._authService.present();
    this._authService.commonApiService('post', 'users', formData).then((result: any) => {
      this.result = result;
      this._authService.dismiss();
      if(this.result != undefined || this.result != null || this.result != {}) {
        this._authService.presentToast('User Added Successfully..!');
        setTimeout(() => {
          this.closeModal();
        }, 3000);
      } else {
        this._authService.presentToast('Failed..!');
      }
    });
  }

  closeModal() {
    this._modalCtrl.dismiss();
}




}
