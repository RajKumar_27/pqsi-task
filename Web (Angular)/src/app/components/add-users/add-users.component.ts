import { Component, OnInit, ViewChild } from '@angular/core';

import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CommonDialogComponent } from 'src/app/common-dialog/common-dialog.component';
import { AuthService } from 'src/app/services/auth/auth.service';


@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.scss']
})
export class AddUsersComponent implements OnInit {


  Colors: string[] = ['red', 'Green'];
  submitted: boolean = false;
  result: any;

  userForm: any = FormGroup;
  dialogValue: any;

  constructor(public _formBuilder: FormBuilder, public _authService: AuthService,
    public dialogRef: MatDialogRef<AddUsersComponent>, public _dialog: MatDialog) { }

  closeDialog() {
    this.dialogRef.close('Pizza!');
  }

  ngOnInit(): void {

    this.userForm = this._formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      avatar: ['https://reqres.in/img/faces/12-image.jpg']
    });
    // this.formList.loadData();
  }

  get formValue() {
    return this.userForm.controls;
  }

  onSubmit() {
    if (this.userForm.invalid) {
      return;
    }
    this.createUser();
  }

  createUser() {
    const formData = new FormData();
    formData.append('first_name', this.formValue.first_name.value);
    formData.append('last_name', this.formValue.last_name.value);
    formData.append('email', this.formValue.email.value);
    formData.append('avatar', this.formValue.avatar.value);

    this._authService.commonApiService('post', 'users', formData).then((result: any) => {
      this.result = result;
      if (this.result != undefined || this.result != null || this.result != {}) {
        var message = 'User Added Successfully..!';
        this.openDialog(message, '3')
      } else {
        alert('Failed..!');
      }
    });
  }

  openDialog(message: string, page: string): void {
    const dialogRef = this._dialog.open(CommonDialogComponent, {
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: { pageValue: page, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.dialogValue = result.data;
      console.log('=----', this.dialogValue);
      if (this.dialogValue == 3) {
        this.closeDialog();
      }
    });


  }

}
