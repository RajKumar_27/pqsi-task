import { Component, OnInit, AfterViewInit, OnChanges, SimpleChange, Input } from '@angular/core';

import { ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

import { MatPaginator } from '@angular/material/paginator'

import { MatSort } from '@angular/material/sort';

import { SelectionModel } from '@angular/cdk/collections';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

 
  displayedColumns: string[] = ['id', 'avatar', 'first_name', 'last_name', 'email'];
  dataSource = new MatTableDataSource();
  selection = new SelectionModel();

  result: any;

  loading = true;

  length: any;
  pageSize: number = 3;
  pageSizeOptions = [3, 6, 12];

  private paginator: any;
  private sort: any;

  
  user_details: any = [];
  total_page: any;

  constructor(
    public _authService: AuthService
    ) {
    
  }

  ngOnInit() {
    this.getUsers();
  }

  ngOnChanges() {
    this.getUsers();
  }

  getUsers() {
    this._authService.commonApiService('get', 'users').then((result: any) => {
      this.result = result;
      
      this.user_details = this.result.data;
      this.dataSource.data = this.user_details;

      this.length = this.user_details.length;
      this.loading = false;
      this.total_page = this.result.total;
      console.log('--', this.result);
    });
  }


  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
  }

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    if (this.paginator && this.sort) {
      // this.applyFilter('');
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  

}
