import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-common-dialog',
  templateUrl: './common-dialog.component.html',
  styleUrls: ['./common-dialog.component.scss']
})
export class CommonDialogComponent implements OnInit {


  fromPage: any;
  fromMessage: any;
  fromDialog: any;

  returnValue: number = 1;

  constructor(public dialogRef: MatDialogRef<CommonDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.fromPage = this.data.pageValue;
    this.fromMessage = this.data.message;
  }

  closeDialog() {
    if (this.fromPage == '1') {
      this.returnValue = 1;
    } else if (this.fromPage == '2') {
      this.returnValue = 2;
    }else if (this.fromPage == '3') {
      this.returnValue = 3;
    }
    this.dialogRef.close({ event: 'close', data: this.returnValue });
  }
}
