import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonDialogComponent } from '../common-dialog/common-dialog.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isSubmitted = false;
  result: any;
  loginForm: any = FormGroup;
  dialogValue: any;
  isBtnSubmitted: boolean = false;

  constructor(private _router: Router, private _formBuilder: FormBuilder,
    public _dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  get formControls() { return this.loginForm.controls; }

  login() {
    let email = 'test@pqsi.in';
    let password = 'pqsi123';
    this.isSubmitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    if (this.formControls.email.value == email && this.formControls.password.value == password) {
      var message = 'Login Successfully...! Welcome our Portal';
      this.isBtnSubmitted = true;
      setTimeout(() => {
        this.isBtnSubmitted = false;
        this.openDialog(message, '1');
      }, 3000);
    
    } else {
      var message = 'Login Failure...! Invalid Email and Password..!';
      this.isBtnSubmitted = true;
      setTimeout(() => {
        this.isBtnSubmitted = false;
        this.openDialog(message, '2');
      }, 3000);
    }

  }

  openDialog(message: string, page: string): void {
    const dialogRef = this._dialog.open(CommonDialogComponent, {
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: { pageValue: page, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.dialogValue = result.data;
      console.log('=----', this.dialogValue);
      if (this.dialogValue == 1) {
        this._router.navigateByUrl('components/dashboard');
      }
    });
  }





}
