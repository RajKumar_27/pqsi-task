import { Injectable } from '@angular/core';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';

import { HttpClient } from '@angular/common/http';

const apiUrl = 'https://reqres.in/api/';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoading = false;

  constructor(public loadingController: LoadingController,
    public toastCtrl: ToastController,
    public alertController: AlertController,
    public _httpClient: HttpClient) { }


  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 3000,
      mode: 'ios',
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  // dismiss loader
  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }
  // present toast
  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 4000,
      color: 'light'
    });
    toast.present();
  }

  async presentAlertConfirm(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Ok',
          id: 'confirm-button',
          handler: (data) => {
            console.log('-----', data)
           return data;
          }
        }
      ]
    });
    await alert.present();
  }


  commonApiService(type: string, api: string, postdata: any = '') {
    if (type == 'post') {
      return new Promise((resolve, reject) => {
        this._httpClient.post(apiUrl + api, postdata,).subscribe(response => {
          resolve(response);
        }, err => {
          reject(err);
        })
      });
    } else {
      return new Promise((resolve, reject) => {
        this._httpClient.get(apiUrl + api + postdata).subscribe((response) => {
          resolve(response);
        }, err => {
          reject(err);
        });
      });
    }
  }
}
