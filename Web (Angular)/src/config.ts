import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class Config {
}

export const apiUrl = 'https://reqres.in/api/';