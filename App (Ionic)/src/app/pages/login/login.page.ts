import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  isBtnSubmitted: boolean = false;
  login_status: any;

  constructor(private _formBuilder: FormBuilder, public _authService: AuthService, public _navCtrl: NavController,
    private _alertController: AlertController, private _router: Router) { }

  ngOnInit() {

    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  get loginFormValues() {
    return this.loginForm.controls;
  }

  login() {
    let email = 'test@pqsi.in';
    let password = 'pqsi123';
    if (this.loginForm.invalid) {
      return;
    }

    if (this.loginFormValues.email.value == email && this.loginFormValues.password.value == password) {
      // alert('success');
      this.isBtnSubmitted = true;
      this._authService.present();
      setTimeout(() => {
        this.isBtnSubmitted = false;
        this._authService.dismiss();
        this.presentAlertConfirm('Status', 'Login Successfully..!');
      }, 3000);
    } else {
      this._authService.presentToast('Invalid Username and Password..!');
    }

  }

  async presentAlertConfirm(title, message) {
    const alert = await this._alertController.create({
      header: title,
      message: message,
      buttons: [
        {
          text: 'Ok',
          id: 'confirm-button',
          handler: (data) => {
            this._navCtrl.navigateRoot('/dashboard');
            // this._router.navigate(['/dashboard']);
          }
        }
      ]
    });
    await alert.present();
  }
}