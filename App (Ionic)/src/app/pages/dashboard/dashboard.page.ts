import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll, ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/service/auth/auth.service';
import { UsersPage } from '../users/users.page';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  result: any;
  user_details: any = [];
  total_page: any;

  constructor(public authService: AuthService, private _modalCtrl: ModalController, public _navCtrl: NavController) { }

  ngOnInit() {
    this.getUsers(1);
  }

  getUsers(page) {
    this.authService.present();
    this.authService.commonApiService('get', 'users?page=' + page).then((result: any) => {
      this.result = result;
      this.authService.dismiss();
      this.user_details = this.result.data;
      this.total_page = this.result.total;
      console.log('--', this.result);
    });
  }
  


  async addUsers() {
    const modal = await this._modalCtrl.create({
      component: UsersPage,
    });
    modal.onWillDismiss().then(data => {
      console.log(data);
      if (data != undefined) {
        console.log(data);
      }
    });
    return await modal.present();
  }

  logout() {
    this._navCtrl.navigateRoot('/login');
  }


  loadMoreData(infiniteScroll) {
    if (this.total_page != this.user_details.length) {
      this.getUsersPagination(2, infiniteScroll);
    } else {
      infiniteScroll.target.complete();
    }
  }

  getUsersPagination(page, infiniteScroll?) {
    this.authService.present();
    this.authService.commonApiService('get', 'users?page=' + page).then((result: any) => {
      this.result = result;
      this.authService.dismiss();
      var user_details = this.result.data;

      this.total_page = this.result.total;
      for (let i = 0; i < user_details.length; i++) {
        const element = user_details[i];
        this.user_details.push(element);
      }
      if (infiniteScroll) {
        infiniteScroll.target.complete();
      }
      console.log('--', this.user_details.length);
    });
  }



}
