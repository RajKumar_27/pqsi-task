import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { apiUrl } from '../../../config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _httpClient: HttpClient) { }

  commonApiService(type: string, api: string, postdata: any = '') {
    if (type == 'post') {
      return new Promise((resolve, reject) => {
        this._httpClient.post(apiUrl + api, postdata,).subscribe(response => {
          resolve(response);
        }, err => {
          reject(err);
        })
      });
    } else {
      return new Promise((resolve, reject) => {
        this._httpClient.get(apiUrl + api + postdata).subscribe((response) => {
          resolve(response);
        }, err => {
          reject(err);
        });
      });
    }
  }
}